<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function hello()
    {
    	return "hello";
    }
    public function hello_world()
    {
    	return "hello world";
    }

    public function new_from_feature()
    {
    	return "new from feature";
    }
}
